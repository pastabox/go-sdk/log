package log_test

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/pastabox/go-sdk/log"
)

func BenchmarkLogger(b *testing.B) {
	l := log.New(io.Discard, false, true).WithFields("boz", 12)

	err := errors.New("ko")

	fns := []func(){
		func() { l.Error("foo", "bar").Msg("baz") },
		func() { l.Error("foo", "bar").Msgf("%s", "baz") },
		func() { l.Error("foo", "bar").Err(err).Msg("baz") },
		func() { l.Err(err).Log("foo", "bar").Msg("baz") },
		func() { l.Err(err).Msg("baz") },
		func() { l.Error().Msg("baz") },
		func() { l.Msg("baz") },
		func() { l.Msg("") },
	}

	for _, fn := range fns {
		b.Run("", func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				fn()
			}
		})
	}
}

func BenchmarkLoggerWithFields(b *testing.B) {
	l := log.New(io.Discard, false, true)

	fns := []func(){
		func() { l.WithFields("foo", "bar", "boz", 12) },
		func() { l.WithFields("foo", "bar") },
		func() { l.WithFields("boz", 12) },
	}

	for _, fn := range fns {
		b.Run("", func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				fn()
			}
		})
	}
}

func BenchmarkLogHTTPRequestMiddleware(b *testing.B) {
	l := log.New(io.Discard, false, false)

	var h http.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
	h = log.LogRequestMiddleware()(h)
	h = log.SetLoggerMiddleware(l)(h)

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		w := httptest.NewRecorder()
		r := httptest.NewRequest("", "/", http.NoBody)
		b.StartTimer()

		h.ServeHTTP(w, r)
	}
}

func BenchmarkLoggerWriters(b *testing.B) {
	file, err := os.CreateTemp("", "")
	if err != nil {
		b.Fatal(err)
	}
	defer file.Close()
	defer os.Remove(file.Name())

	cases := []struct {
		desc   string
		writer io.Writer
	}{
		{"discard", io.Discard},
		{"buffer", &bytes.Buffer{}},
		{"file", file},
	}

	for _, c := range cases {
		b.Run(c.desc, func(b *testing.B) {
			l := log.New(c.writer, false, false).WithFields("foo", "bar")
			e := errors.New("ko")

			for i := 0; i < b.N; i++ {
				l.Err(e).Log("baz", 12).Msg("failed to...")
			}
		})
	}
}
