package log

import (
	"net/http"
	"time"
)

type Logger interface {
	WithFields(v ...any) Logger
	WithCaller() Logger

	Panic(v ...any) Entry
	Fatal(v ...any) Entry
	Error(v ...any) Entry
	Warn(v ...any) Entry
	Info(v ...any) Entry
	Debug(v ...any) Entry

	Entry

	Write(p []byte) (n int, err error)

	LogHTTPRequest(r *http.Request, statusCode int, requestTime time.Duration)
}

type Entry interface {
	Err(err error) Entry
	Log(v ...any) Entry

	Msg(msg string)
	Msgf(fmt string, v ...any)
}
