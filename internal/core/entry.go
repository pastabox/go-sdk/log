package core

import (
	"github.com/rs/zerolog"

	"gitlab.com/pastabox/go-sdk/log/internal/log"
)

type Entry struct{ ze *zerolog.Event }

type et = Entry
type ei = log.Entry

func (o et) Err(err error) ei { o.ze.Err(err); return o }
func (o et) Log(v ...any) ei  { o.ze.Fields(v); return o }

func (o et) Msg(msg string)            { o.ze.Msg(msg) }
func (o et) Msgf(fmt string, v ...any) { o.ze.Msgf(fmt, v...) }
