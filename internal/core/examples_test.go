package core_test

import (
	"errors"
	"net/http/httptest"
	"os"
	"time"

	"gitlab.com/pastabox/go-sdk/log/internal/core"
	"gitlab.com/pastabox/go-sdk/log/internal/log"
)

func makeLogs(l log.Logger) {
	e := errors.New("ko")
	v := []any{"foo", "bar", "baz", 12}
	m := "boz"

	l.WithFields(v...).Msg(m)
	l.WithFields(v...).Msgf("%s", m)

	func() { defer func() { _ = recover() }(); l.Panic(v...).Err(e).Msg(m) }()
	func() { defer func() { _ = recover() }(); l.Panic(v...).Err(e).Msgf("%s", m) }()

	l.Error(v...).Err(e).Msg(m)
	l.Error(v...).Err(e).Msgf("%s", m)

	l.Warn(v...).Err(e).Msg(m)
	l.Warn(v...).Err(e).Msgf("%s", m)

	l.Info(v...).Err(e).Msg(m)
	l.Info(v...).Err(e).Msgf("%s", m)

	l.Debug(v...).Err(e).Msg(m)
	l.Debug(v...).Err(e).Msgf("%s", m)

	l.Log(v...).Err(e).Msg(m)
	l.Log(v...).Err(e).Msgf("%s", m)

	l.Err(e).Log(v...).Msg(m)
	l.Err(e).Log(v...).Msgf("%s", m)

	_, _ = l.Write([]byte(m))

	l.LogHTTPRequest(httptest.NewRequest("PUT", "/target", nil), 200, 1234*time.Microsecond)
}

func ExampleLogger() {
	makeLogs(core.New(os.Stdout, false, true))

	// Output:
	// {"foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"lvl":"panic","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"panic","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"error","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"error","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"warn","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"warn","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"info","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"info","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"debug","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"debug","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"error","err":"ko","foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"lvl":"error","err":"ko","foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"ts":1355314332,"msg":"boz"}
	// {"proto":"HTTP/1.1","method":"PUT","path":"/target","remote":"192.0.2.1:1234","ua":"","code":200,"time":1.234,"ts":1355314332}
}

func ExampleLogger_noDebug() {
	makeLogs(core.New(os.Stdout, false, false))

	// Output:
	// {"foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"lvl":"panic","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"panic","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"error","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"error","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"warn","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"warn","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"info","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"info","foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"foo":"bar","baz":12,"err":"ko","ts":1355314332,"msg":"boz"}
	// {"lvl":"error","err":"ko","foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"lvl":"error","err":"ko","foo":"bar","baz":12,"ts":1355314332,"msg":"boz"}
	// {"ts":1355314332,"msg":"boz"}
	// {"proto":"HTTP/1.1","method":"PUT","path":"/target","remote":"192.0.2.1:1234","ua":"","code":200,"time":1.234,"ts":1355314332}
}

func ExampleLogger_nop() {
	makeLogs(core.Nop())

	// Output:
}
