package core_test

import (
	"time"

	"github.com/rs/zerolog"
)

func init() {
	var testTimestamp = time.Date(2012, 12, 12, 12, 12, 12, 0, time.UTC)
	zerolog.TimestampFunc = func() time.Time { return testTimestamp }
}
