package core

import (
	"io"
	"net/http"
	"time"

	"github.com/rs/zerolog"

	"gitlab.com/pastabox/go-sdk/log/internal/log"
)

type Logger struct{ l zerolog.Logger }

type lt = Logger
type li = log.Logger

func New(w io.Writer, pretty, debug bool) Logger {
	if pretty {
		w = zerolog.NewConsoleWriter(
			func(cw *zerolog.ConsoleWriter) { cw.Out = w },
		)
	}

	level := zerolog.InfoLevel
	if debug {
		level = zerolog.DebugLevel
	}

	return Logger{zerolog.New(w).Level(level).With().Timestamp().Logger()}
}

func Nop() Logger {
	return Logger{zerolog.Nop()}
}

func (o lt) WithFields(v ...any) li { return lt{o.l.With().Fields(v).Logger()} }
func (o lt) WithCaller() li         { return lt{o.l.With().CallerWithSkipFrameCount(3).Logger()} }

func (o lt) Panic(v ...any) ei { return et{o.l.Panic().Fields(v)} }
func (o lt) Fatal(v ...any) ei { return et{o.l.Fatal().Fields(v)} }
func (o lt) Error(v ...any) ei { return et{o.l.Error().Fields(v)} }
func (o lt) Err(err error) ei  { return et{o.l.Error().Err(err)} }
func (o lt) Warn(v ...any) ei  { return et{o.l.Warn().Fields(v)} }
func (o lt) Info(v ...any) ei  { return et{o.l.Info().Fields(v)} }
func (o lt) Debug(v ...any) ei { return et{o.l.Debug().Fields(v)} }
func (o lt) Log(v ...any) ei   { return et{o.l.Log().Fields(v)} }

func (o lt) Msg(msg string)            { o.l.Log().Msg(msg) }
func (o lt) Msgf(fmt string, v ...any) { o.l.Log().Msgf(fmt, v...) }

func (o lt) Write(p []byte) (int, error) { return o.l.Write(p) }

func (o lt) LogHTTPRequest(r *http.Request, statusCode int, requestTime time.Duration) {
	o.l.Log().
		Str("proto", r.Proto).
		Str("method", r.Method).
		Str("path", r.URL.Path).
		Str("remote", r.RemoteAddr).
		Str("ua", r.UserAgent()).
		Int("code", statusCode).
		Dur("time", requestTime).
		Msg("")
}
