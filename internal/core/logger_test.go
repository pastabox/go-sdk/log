package core_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"runtime"
	"testing"

	"gitlab.com/pastabox/go-sdk/log/internal/core"
)

func TestPanic(t *testing.T) {
	var didPanic bool

	func() {
		defer func() { _ = recover() }()

		didPanic = true
		core.Nop().Panic().Msg("")
		didPanic = false
	}()

	if !didPanic {
		t.Error("did not panic")
	}
}

func TestWithCaller(t *testing.T) {
	b := bytes.Buffer{}
	l := core.New(&b, false, false).WithCaller()

	l.Msg("")
	_, file, line, _ := runtime.Caller(0)

	var log struct{ Caller *string }

	err := json.NewDecoder(&b).Decode(&log)
	switch {
	case err != nil:
		t.Fatal(err)
	case log.Caller == nil:
		t.Fatal(`no field "caller"`)
	}

	expect := fmt.Sprintf("%s:%d", file, line-1)
	if *log.Caller != expect {
		t.Error("expected: " + expect)
	}
}
