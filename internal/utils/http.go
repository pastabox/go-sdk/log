package utils

import "net/http"

type responseCodeRecorder struct {
	http.ResponseWriter
	Code int
}

func NewResponseCodeRecorder(w http.ResponseWriter) *responseCodeRecorder {
	return &responseCodeRecorder{w, http.StatusOK}
}

func (w *responseCodeRecorder) WriteHeader(statusCode int) {
	w.ResponseWriter.WriteHeader(statusCode)
	w.Code = statusCode
}
