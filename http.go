package log

import (
	"net/http"
	"time"

	"gitlab.com/pastabox/go-sdk/log/internal/utils"
)

func SetLoggerMiddleware(l Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := ContextWithLogger(r.Context(), l)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

const RequestIDHeaderKey = "request-id"

func GetRequestID(r *http.Request) string     { return r.Header.Get(RequestIDHeaderKey) }
func SetRequestID(r *http.Request, id string) { r.Header.Set(RequestIDHeaderKey, id) }

func SetRequestIDMiddleware(newid func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			id := GetRequestID(r)
			if id == "" {
				id = newid()
				SetRequestID(r, id)
			}

			ctx := r.Context()
			ctx = ContextWithLogger(ctx, ContextLogger(ctx).WithFields("requestid", id))

			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(fn)
	}
}

func LogRequestMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			rec, now := utils.NewResponseCodeRecorder(w), time.Now()
			next.ServeHTTP(rec, r)
			ContextLogger(r.Context()).LogHTTPRequest(r, rec.Code, time.Since(now))
		}

		return http.HandlerFunc(fn)
	}
}
