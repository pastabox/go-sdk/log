package log

import (
	"io"

	"gitlab.com/pastabox/go-sdk/log/internal/core"
	"gitlab.com/pastabox/go-sdk/log/internal/log"
)

type Logger = log.Logger

func New(w io.Writer, pretty, debug bool) Logger { return core.New(w, pretty, debug) }
func Nop() Logger                                { return core.Nop() }
