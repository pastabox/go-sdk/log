package log_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/pastabox/go-sdk/log"
)

func TestMiddlewares(t *testing.T) {
	const requestID = "deadb33f"

	b := bytes.Buffer{}
	l := log.New(&b, false, false)
	w := httptest.NewRecorder()
	r := httptest.NewRequest("PUT", "/target", http.NoBody)

	var h http.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if id := r.Header.Get(log.RequestIDHeaderKey); id != requestID {
			t.Fatal("expected", requestID, "got", id)
		}

		w.WriteHeader(http.StatusTeapot)
	})

	h = log.LogRequestMiddleware()(h)
	h = log.SetRequestIDMiddleware(func() string { return requestID })(h)
	h = log.SetLoggerMiddleware(l)(h)

	h.ServeHTTP(w, r)

	var log map[string]any
	if err := json.NewDecoder(&b).Decode(&log); err != nil {
		t.Fatal(err)
	}

	for field, expected := range map[string]any{
		"requestid": requestID, "proto": "HTTP/1.1", "method": "PUT", "path": "/target", "code": 418., "time": log["time"],
	} {
		switch actual, exists := log[field]; {
		case !exists:
			t.Fatal("field", field, "not found")
		case actual != expected:
			t.Fatal("expected", expected, "got", actual)
		}
	}
}
